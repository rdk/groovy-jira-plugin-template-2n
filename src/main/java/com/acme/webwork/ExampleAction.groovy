package com.acme.webwork

import com.acme.utils.DatePickerUtils
import com.acme.utils.GenericQuery
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory
import com.atlassian.jira.propertyset.JiraPropertySetFactory
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.action.JiraWebActionSupport
import com.atlassian.jira.web.action.util.CalendarResourceIncluder
import com.google.common.collect.ImmutableList
import com.opensymphony.module.propertyset.PropertySet
import org.ofbiz.core.entity.GenericValue

import java.sql.Connection

/**
 *
 */
public class ExampleAction extends JiraWebActionSupport {

    private static final String SECURITYBREACH_RESULT = "securitybreach";
    private static final String LIST = "list";
    private static final String VIEW = "view";

    private static final String ADMIN_GROUP = "jira-administrators";

    Map<String, Object> dp = new HashMap<>()  // datepicker params



    public static boolean isCurrentUserInGroup(String groupName) {
        ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
        return ComponentAccessor.getGroupManager().isUserInGroup(user.getName(), groupName);
    }

    private boolean checkPermissions() {
        return isCurrentUserInGroup(ADMIN_GROUP);
    }


    CalendarResourceIncluder getCalendarIncluder() {
        dp.get("calendarIncluder")
    }


//===========================================================================================================//

    // OfBiz ORM:

    private static final List<String> COMMENT_FIELDS_TO_SELECT = ImmutableList.of("author","created");
    private static final List<String> COMMENT_ORDER_BY = ImmutableList.of("created ASC");

    private static boolean isUserDeveloper(String userName) {
        return ComponentAccessor.getGroupManager().isUserInGroup(userName, "jira-developers");
    }


    private static Date getFirstDeveloperCommentTimeGVOptimized(Issue issue) {
        if (issue==null) return null;

        List<GenericValue> comments = GenericQuery.where()
                .addEquals("issue", issue.getId())
                .addEquals("type", "comment")
                .addEquals("level", null)   //grouplevel?
                .addEquals("rolelevel", null)
                .findByAnd("Action", COMMENT_FIELDS_TO_SELECT, COMMENT_ORDER_BY);


        for (GenericValue comment : comments) {
            if (isUserDeveloper(comment.getString("author"))) {
                return comment.getTimestamp("created");
            }
        }

        return null;
    }


//===========================================================================================================//

    // Entity properties (for Issue, Project, etc...)

    private PropertySet getPropertySetForEntity(String entityName, Long entityId) {
        JiraPropertySetFactory psf = ComponentAccessor.getComponent(JiraPropertySetFactory.class);
        PropertySet propertySet = psf.buildNoncachingPropertySet(entityName, entityId);
        return propertySet;
    }

//===============================================================================================//

    @Override
    public String doExecute() throws Exception {
        if (!checkPermissions()) {
            return SECURITYBREACH_RESULT;
        }

        ComponentAccessor.getWebResourceManager().requireResourcesForContext("jira.browse")
        ComponentAccessor.getWebResourceManager().requireResourcesForContext("jira.create.issue")   // doesn't seem to work

        DatePickerUtils.addDatePickerVelocityParams(dp)


        // get JIRA SQL connection
        Connection conn = new DefaultOfBizConnectionFactory().getConnection(); // Instead of JDBS or JNDI, this factory contains in jira-core library


        return VIEW;
    }


    public String doUpdate() throws Exception {
        if (!checkPermissions()) {
            return ERROR;
        }

        log.debug("UPDATE")

        return getRedirect("/secure/ExampleAction.jspa");
    }


}
