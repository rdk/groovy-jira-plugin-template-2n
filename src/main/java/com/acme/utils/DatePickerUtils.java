package com.acme.utils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.jira.web.action.util.CalendarResourceIncluder;

import java.util.Map;

public class DatePickerUtils {
	public static void addDatePickerVelocityParams(Map<String, Object> params) {
		
        final String language = ComponentAccessor.getComponent(JiraAuthenticationContext.class).getLocale().getLanguage();
        params.put("hasCalendarTranslation", ComponentAccessor.getComponent(CalendarLanguageUtil.class).hasTranslationForLanguage(language));
        params.put("calendarIncluder", new CalendarResourceIncluder());
		
        // Add javascript date time format params
        params.put("dateFormat", DateTimeFormatUtils.getDateFormat());
        params.put("dateTimeFormat", DateTimeFormatUtils.getDateTimeFormat());
        params.put("timeFormat", DateTimeFormatUtils.getTimeFormat());
        
	}
}
