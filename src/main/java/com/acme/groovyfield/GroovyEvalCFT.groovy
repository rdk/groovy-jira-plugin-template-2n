package com.acme.groovyfield

import com.atlassian.jira.issue.customfields.impl.FieldValidationException
import com.atlassian.jira.issue.customfields.impl.URLCFType
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister
import groovy.transform.CompileStatic
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer

/**
 * Customfield that evaluates its text value as groovy code.
 * Result is shown in view template.
 *
 * example value: "issue.issueTypeObject.name + ': ' + issue.summary"
 */
@CompileStatic
class GroovyEvalCFT extends URLCFType {

    GroovyEvalCFT(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager)
    }

    public String getSingularObjectFromString(final String string) throws FieldValidationException {
        // JRA-14998 - trim URLs before validating. URLs will also be saved in trim form.
        final String groovycode = (string == null) ? null : string.trim();

        return groovycode;
    }

    /**
     * used in view template: templates/groovyfield/groovyfield-view.vm
     */
    String evalGroovy(String code, Map ctx) {

        def imports = new ImportCustomizer()
        //imports.addImport(Params.class.simpleName, Params.class.name )

        def customizer = new CompilerConfiguration()
        customizer.addCompilationCustomizers(imports)
        Map vars = new HashMap(ctx)
        Binding binding = new Binding(vars)
        GroovyShell shell = new GroovyShell(binding, customizer)

        try {
            return shell.evaluate(code)
        } catch (Exception e) {
            def msg = "Error: " + e.message
            log.error(msg, e)
            return msg
        }
    }
}
