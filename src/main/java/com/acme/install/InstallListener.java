package com.acme.install;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.extension.Startable;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class InstallListener implements  InitializingBean, DisposableBean, Startable {

	private static final Logger log = LoggerFactory.getLogger(InstallListener.class);

	public static final String PLUGIN_KEY = "com.acme.groovy-jira-plugin-csas-examples";

    private final PluginEventManager pluginEventManager;

	public InstallListener(PluginEventManager pluginEventManager) {
       this.pluginEventManager = pluginEventManager;
   }

	@Override
	public void destroy() throws Exception {
		log.debug("InstallListener destroy()");

		pluginEventManager.unregister(this);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("InstallListener afterPropertiesSet()");

		pluginEventManager.register(this);
	}

	@PluginEventListener
	public void onPluginEnabled(PluginEnabledEvent event) {
		if( event.getPlugin().getKey().equals(PLUGIN_KEY) ){
			log.debug("PluginEnabled = " + event.getPlugin().getKey());

			@SuppressWarnings("deprecation")
			ComponentManager componentManager = ComponentManager.getInstance();
			
			if( componentManager != null){
				log.debug( "is Plugin System Started = " + componentManager.getState().isStarted());
				if ( componentManager.getState().isStarted()){
					setupAddon();
				}
			}
		}
	}
	@Override
	public void start() throws Exception {
		log.debug("FarAway start actions");
		setupAddon();
	}

	private void setupAddon() {
		try {

			// plugin setup code here (create customfields, check required config etc.)

		} catch (Exception e) {
			log.error("Install error - " + e.getMessage());
		}
	}
}
