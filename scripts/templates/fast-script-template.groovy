//=================================================================//
class g {static String msg = ""}
def print(String s) { log.warn s; g.msg += s }
def println(String s) { print s + "\n" }
//=================================================================//

println "DONE"

//=================================================================//
return g.msg.replaceAll("\n", "<br>")

