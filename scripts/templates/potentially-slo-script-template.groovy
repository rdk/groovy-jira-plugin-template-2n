import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.util.JiraHome
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.thread.JiraThreadLocalUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory


//===========================================================================================================//

abstract class ScriptSupport {
    static Logger log = LoggerFactory.getLogger(ScriptSupport.class)
    String outputFileNamePrefix = "script_output_"
    String outputFileName
    StringBuilder msg = new StringBuilder(10000)
    PrintWriter writer
    boolean previewMode = true
    boolean runInBackground = false
    boolean coloerdOutput = true
    ApplicationUser ACTOR = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()



    ScriptSupport() {
        outputFileName = jiraHomePath + "/tmp/" + outputFileNamePrefix + System.currentTimeMillis() + ".log"
    }

    def getJiraHomePath() {
        ComponentAccessor.getComponentOfType(JiraHome.class).getHome().absolutePath
    }

    def print(String s) {
        msg.append(s)
        log.warn s
        if (writer!=null) {
            writer.print(s)
            writer.flush()
        }
    }
    def println(String s) { print s + "\n" }

    def write(String s) {
        print s
    }
    def writeln(String s) { println s }


    def doResult() {
        try {
            writer = overwrite(outputFileName)
            println "writing output to file '$outputFileName'"
        } catch (Exception e) {
            log.error(e.message, e)
        }

        execute()

        if (writer!=null) {
            writer.close()
        }

        return formatMessages(msg)
    }

    def formatMessages(CharSequence msgs) {
        String msg
        if (coloerdOutput) {
            msg = colorifyLines(msgs)
        } else {
            msg = msgs.toString()
        }
        colorifyLines(msgs)
        msg = msg.replaceAll("\n", "<br>")
        msg = "<pre><b>$msg</b></pre>"

        return msg
    }

    def result() {
        def result = null
        if (!runInBackground) {
            result = doResult()
        } else {
            def thread = new Thread(JiraThreadLocalUtils.wrap(new Runnable() {
                @Override
                void run() {
                    ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ACTOR)
                    result = doResult()
                }
            }))
            thread.start()
            try {
                thread.wait()
            } catch (e) {
            }
            result = result ?: "running in background... log: $outputFileName"
        }
        return result
    }


    def prefixLines(String prefix, String lines) {
        if (lines==null) return null
        lines.readLines().collect {prefix+it}.join("\n")
    }
    String colorifyLines(CharSequence messages) {
        StringBuilder res = new StringBuilder(messages.size())
        for (String line : messages.readLines()) {
            if (line.contains("[ERROR]")) line = "<span style='color: #d04437;'>" + line + "</span>"
            if (line.contains("[WARN]")) line = "<span style='color: #f5bb2a;'>" + line + "</span>"
            if (line.contains("[OK]")) line = "<span style='color: #14892c;'>" + line + "</span>"

            res.append(line + "\n")
        }
        return res.toString()
    }

    static PrintWriter overwrite(String fname) {
        File file = new File(fname)
        if (file.exists()) {
            file.delete()
        }
        file.createNewFile()

        return new PrintWriter(new BufferedWriter(new FileWriter(file), 10000))
    }


    abstract void execute()
}

//===========================================================================================================//


class JiraConfigHelper {

    private Map<String, IssueType> issueTypesByName = new HashMap<>()

    public JiraConfigHelper() {
        for (IssueType issueType : ComponentAccessor.getConstantsManager().getAllIssueTypeObjects()) {
            issueTypesByName.put(issueType.getName(), issueType)
        }
    }

    public IssueType getIssueTypeByName(String name) {
        return issueTypesByName.get(name)
    }

}



//===========================================================================================================//

/**
 * main business logic of the script
 */
class MigrationScript extends ScriptSupport {

    String param1

    @Override
    void execute() {
        // write script logic here
        if (!previewMode) {
            // write to DB
        }
        println "DONE."
    }
}


//========================================================================================================

MigrationScript script = new MigrationScript()

//--[EDIT THIS]--------------------------------------------------
script.runInBackground = true   // pokud je true spusti se ve vlaknu na pozadi
script.previewMode = true        // pokud je true zadne zmeny nebudou zapsany

script.param1 = "xx"

//---------------------------------------------------------------

result = script.result()






