import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.SubTaskManager
import com.atlassian.jira.config.properties.APKeys
import com.atlassian.jira.config.util.JiraHome
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.customfields.option.Option
import com.atlassian.jira.issue.customfields.option.Options
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.config.FieldConfig
import com.atlassian.jira.issue.fields.config.FieldConfigScheme
import com.atlassian.jira.issue.history.ChangeLogUtils
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.issue.index.IssueIndexingParams
import com.atlassian.jira.issue.index.IssueIndexingService
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.issue.label.Label
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.jql.parser.JqlParseException
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.jql.util.JqlStringSupport
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.DateFieldFormat
import com.atlassian.jira.util.thread.JiraThreadLocalUtils
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import com.google.common.base.Splitter
import com.google.common.base.Throwables
import org.apache.commons.lang3.ObjectUtils
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.function.Consumer

import static com.atlassian.jira.component.ComponentAccessor.getCustomFieldManager
import static java.util.Collections.emptyList
import static java.util.Collections.emptySet
import static java.util.Objects.requireNonNull

/**
 *  process-issues-template.groovy
 *
 *  Skript pro davkove zpracovani velkeho poctu issues.
 *
 *  v 1.2
 */

//=================================================================//

class ATimer {

    private long start

    private ATimer() {}

    public ATimer(long start) {
        this.start = start
    }

    public ATimer reset() {
        start = now()
        return this
    }

    /**
     * @return time from (re)start in millis
     */
    public long getTime() {
        return System.currentTimeMillis() - start
    }

    public long getTimeAndReset() {
        long now = now()
        long res = now - start
        start = now
        return res
    }

    public static ATimer start() {
        return new ATimer().reset()
    }

    public static long now() {
        return System.currentTimeMillis()
    }

}

/**
 * Helper for issue searches and processing a lot of issues in batches.
 *
 * Searches all issues disregarding issue permissions / security.
 */
class SearchHelper {
    private static final Logger log = LoggerFactory.getLogger(SearchHelper.class)

    SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class)
    SearchService searchService = ComponentAccessor.getComponent(SearchService.class)

    public static Query parseJqlQuery(String jql) {
        try {
            JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class)
            return jqlQueryParser.parseQuery(jql)
        } catch (JqlParseException e) {
            throw new RuntimeException(e)
        }
    }

    /**
     *
     * @param jql
     * @return
     */
    public long getCount(Query jql) {
        try {
            return searchService.searchCountOverrideSecurity((ApplicationUser) null, jql)
        } catch (Exception e) {
            throw new RuntimeException(e)
        }
    }

    /**
     *
     * @param jql
     * @return
     */
    public List<Issue> getIssues(Query jql) {
        try {
            return searchService.searchOverrideSecurity((ApplicationUser) null, jql, PagerFilter.getUnlimitedFilter()).getResults()
        } catch (Exception e) {
            throw new RuntimeException(e)
        }
    }

    public Issue getSingleIssue(Query jql) {
        List<Issue> issues = getIssues(jql)

        if (issues==null || issues.isEmpty()) {
            return null
        } else {
            if (issues.size()>1) {
                log.warn("More than one issue found (should be single)! query:[{}]", jql.getQueryString())
            }
            return issues.get(0)
        }
    }

    /**
     *
     * @param jql
     * @param batchSize
     * @param callback
     */
    public void processBatches(Query jql, int batchSize, BatchProcessor callback) {

        log.debug("JQL: " + ComponentAccessor.getComponent(JqlStringSupport.class).generateJqlString(jql))

        long count = getCount(jql)
        int pos = 0

        log.debug("count: " + count)

        while (pos<count) {
            log.debug("processing batch: [start:{},max:{}]", pos, batchSize)
            try {
                SearchResults<Issue> searchRes = searchService.searchOverrideSecurity((ApplicationUser) null, jql, new PagerFilter<>(pos, batchSize))
                log.debug("batch size: {}", searchRes.getResults().size())

                logIssues(searchRes)

                if (!searchRes.getResults().isEmpty()) {
                    callProcessIssues(batchSize, callback, pos, searchRes)
                } else {
                    log.warn("Search returned batch with 0 issues (this shouldn't happen!)")
                }

                pos = pos + batchSize
            } catch (SearchException e) {
                throw new RuntimeException(e)
            }
        }

    }

    private void logIssues(SearchResults<Issue> searchRes) {
        if (log.isTraceEnabled()) {
            StringBuilder s = new StringBuilder("\n")
            for (Issue issue : searchRes.getResults()) {
                s.append(issue.getKey())
                s.append("\n")
            }
            log.trace("Issues in batch:"+s.toString())
        }
    }

    private void callProcessIssues(int batchSize, BatchProcessor callback, int pos, SearchResults<Issue> searchRes) {
        try {
            callback.processIssues(searchRes.getResults())
        } catch (Exception e) {
            String chs =  "[start:"+pos+",size:"+batchSize+"]"
            log.error("Failure during processing batch: "+chs, e)
        }
    }

    /**
     *
     */
    public static abstract class BatchProcessor {
        public abstract void processIssues(List<Issue> issues);
    }

    public static abstract class IssueProcessor {
        public abstract void processIssue(Issue issue);
    }

    public static void processIssuesInBatches(Query jql, int batchSize, IssueProcessor processor) {
        new SearchHelper().processBatches(jql, batchSize, new BatchProcessor() {
            @Override
            public void processIssues(List<Issue> issues) {
                for (Issue issue : issues) {
                    try {
                        processor.processIssue(issue)
                    } catch (Exception e) {
                        log.error("Failed to process issue "+issue.getKey(), e)
                    }
                }
            }
        })
    }

}


/**
 * Updater of custom field values on issue.
 */
public class CustomFieldUpdater {

    private Issue issue
    private List<Item> items = new LinkedList<>()
    private ApplicationUser changeAuthor = null  // may be null


    private CustomFieldUpdater() {}

    /**
     *
     * @param issue
     * @return
     */
    public static CustomFieldUpdater forIssue(Issue issue) {
        CustomFieldUpdater updater = new CustomFieldUpdater()
        updater.issue = issue
        return updater
    }

    public CustomFieldUpdater withAuthor(ApplicationUser changeAuthor) {
        this.changeAuthor = changeAuthor
        return this
    }

    /**
     *
     * @param cf
     * @param newValue
     * @return
     */
    public CustomFieldUpdater addItem(CustomField cf, Object newValue) {
        if (cf!=null) {
            items.add(new Item(cf, newValue))
        }
        return this
    }

    /**
     * does reindex automatically
     *
     * @param withHistory
     */
    public void update(boolean withHistory) {

        DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder()

        for (Item item : items) {
            CustomField customField = item.customField
            Object newValue = item.newValue
            Object oldValue = issue.getCustomFieldValue(customField)

            try {
                if (ObjectUtils.notEqual(newValue, oldValue)) {
                    customField.updateValue(null, issue, new ModifiedValue<>(issue.getCustomFieldValue(customField), newValue), changeHolder)
                }
            } catch (Exception e) {
                //log.error("error on updating field " + customField.getId(), e);
                new RuntimeException("Error on updating field " + customField.getId(), e)
            }

        }

        if (withHistory) {

            try {
                ChangeLogUtils.createChangeGroup(
                        changeAuthor,
                        issue,
                        issue,
                        changeHolder.getChangeItems(),
                        true
                )
            } catch (Exception e) {
                //log.error("error on creating change history", e);
                new RuntimeException("Error on creating change history", e)
            }

        }

        // reindex issue?

    }

    /**
     *
     */
    public void updateWithHistory() {
        update(true)
    }

    /**
     *
     */
    static class Item {
        CustomField customField
        Object newValue

        public Item(CustomField customField, Object newValue) {
            this.customField = customField
            this.newValue = newValue
        }

        public CustomField getCustomField() {
            return customField
        }

        public Object getNewValue() {
            return newValue
        }
    }

    private static final Logger log = LoggerFactory.getLogger(CustomFieldUpdater.class)
}

//===========================================================================================================//

abstract class ScriptSupport {
    static Logger log = LoggerFactory.getLogger(ScriptSupport.class)
    String outputFileNamePrefix = "script_output_"
    String outputFileName
    StringBuilder msg = new StringBuilder(10000)
    PrintWriter writer
    boolean previewMode = true
    boolean runInBackground = false
    boolean coloerdOutput = true
    ApplicationUser ACTOR = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()



    ScriptSupport() {
        outputFileName = jiraHomePath + "/tmp/" + outputFileNamePrefix + System.currentTimeMillis() + ".log"
    }

    def getJiraHomePath() {
        ComponentAccessor.getComponentOfType(JiraHome.class).getHome().absolutePath
    }

    def print(String s) {
        msg.append(s)
        log.warn s
        if (writer!=null) {
            writer.print(s)
            writer.flush()
        }
    }
    def println(String s) { print s + "\n" }

    def write(String s) {
        print s
    }
    def writeln(String s) { println s }


    def doResult() {
        try {
            writer = overwrite(outputFileName)
            println "writing output to file '$outputFileName'"
        } catch (Exception e) {
            log.error(e.message, e)
        }

        execute()

        if (writer!=null) {
            writer.close()
        }

        return formatMessages(msg)
    }

    def formatMessages(CharSequence msgs) {
        String msg 
        if (coloerdOutput) {
            msg = colorifyLines(msgs)
        } else {
            msg = msgs.toString()
        }
                colorifyLines(msgs)
        msg = msg.replaceAll("\n", "<br>")
        msg = "<pre><b>$msg</b></pre>"

        return msg
    }

    def result() {
        def result = null
        if (!runInBackground) {
            result = doResult()
        } else {
            def thread = new Thread(JiraThreadLocalUtils.wrap(new Runnable() {
                @Override
                void run() {
                    ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ACTOR)
                    result = doResult()
                }
            }))
            thread.start()
            try {
                thread.wait()
            } catch (e) {
            }
            result = result ?: "running in background... log: $outputFileName"
        }
        return result
    }


    def prefixLines(String prefix, String lines) {
        if (lines==null) return null
        lines.readLines().collect {prefix+it}.join("\n")
    }
    String colorifyLines(CharSequence messages) {
        StringBuilder res = new StringBuilder(messages.size())
        for (String line : messages.readLines()) {
            if (line.contains("[ERROR]")) line = "<span style='color: #d04437;'>" + line + "</span>"
            if (line.contains("[WARN]")) line = "<span style='color: #f5bb2a;'>" + line + "</span>"
            if (line.contains("[OK]")) line = "<span style='color: #14892c;'>" + line + "</span>"

            res.append(line + "\n")
        }
        return res.toString()
    }

    static PrintWriter overwrite(String fname) {
        File file = new File(fname)
        if (file.exists()) {
            file.delete()
        }
        file.createNewFile()

        return new PrintWriter(new BufferedWriter(new FileWriter(file), 10000))
    }


    abstract void execute()
}

//===========================================================================================================//


class JiraConfigHelper {

    private Map<String, IssueType> issueTypesByName = new HashMap<>()

    public JiraConfigHelper() {
        for (IssueType issueType : ComponentAccessor.getConstantsManager().getAllIssueTypeObjects()) {
            issueTypesByName.put(issueType.getName(), issueType)
        }
    }

    public IssueType getIssueTypeByName(String name) {
        return issueTypesByName.get(name)
    }

}

abstract class IssueProcessingScript extends ScriptSupport {
    JiraConfigHelper configHelper = new JiraConfigHelper()

    SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
    IssueService issueService = ComponentAccessor.issueService
    IssueLinkManager issueLinkManager = ComponentAccessor.issueLinkManager
    IssueLinkTypeManager issueLinkTypeManager = ComponentAccessor.getComponent(IssueLinkTypeManager.class)
    DateFieldFormat dateFieldFormat = ComponentAccessor.getComponent(DateFieldFormat.class)
    SubTaskManager subTaskManager = ComponentAccessor.subTaskManager
    IssueIndexManager issueIndexManager = ComponentAccessor.getComponent(IssueIndexManager.class)
    IssueIndexingService issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class)
    String JIRA_BASE_URL = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL)

    int ISSUE_BATCH_SIZE = 500

    String JQL_QUERY = null
    Consumer<Issue> issueConsumer

    IssueProcessingScript() {
        super()
        this.issueConsumer = getIssueProcessor()
    }

    abstract Consumer<Issue> getIssueProcessor();

    abstract void preCall();
    abstract void postCall();

    static String printException(Throwable t) {
        return t.message + "\n" + Throwables.getStackTraceAsString(t)
    }

    public static CustomField getCFByName(String name) {
        return customFieldManager.getCustomFieldObjectByName(name)
    }
    public static Object getCFValue(String name, Issue issue) {
        CustomField field = getCFByName(name)

        if (field==null) {
            println "[ERROR] Custom field '$name' doesn't exist!"
            return null
        } else {
            return field.getValue(issue)
        }
    }


    public static String cf(String customFieldName) {
        return getCFByName(customFieldName)?.id
    }

    def reindexIssue(Issue issue) {
        if (issue==null) return

        try {
            issueIndexingService.reIndex(issue, IssueIndexingParams.INDEX_ISSUE_ONLY)
            //println "[OK] Reindexed $issue.key"
        } catch (Exception e) {
            println "[ERROR] Failed to reindex ${issue.key}: " + printException(e)
        }
    }


    void execute() {

        def timer = ATimer.start()

        writeln "PREVIEW MODE: " + previewMode

        Query query = SearchHelper.parseJqlQuery(JQL_QUERY)

        writeln "Processing ${new SearchHelper().getCount(query)} issues"
        writeln '\n'


        preCall()

        SearchHelper.processIssuesInBatches(query, ISSUE_BATCH_SIZE, new SearchHelper.IssueProcessor() {
            @Override
            void processIssue(Issue issue) {
                try {
                    issueConsumer.accept(issue)
                } catch (Exception e) {
                    writeln "[ERROR]: " + e.getMessage() + "\n" + Throwables.getStackTraceAsString(e)
                }
            }
        })

        postCall()

        writeln "\nFinished in ${timer.time / (1000*60)} min"
        writeln '\nDONE.'
    }
}

//===========================================================================================================//

/**
 * main business logic of the script
 */
class MigrationScript extends IssueProcessingScript {

    int updatedIssues = 0

    @Override
    void preCall() {
        writeln "Processing issues..."
    }


    @Override
    Consumer<Issue> getIssueProcessor() {
        return new Consumer<Issue>() {
            @Override
            void accept(Issue issue) {
                write "$issue.key ($issue.issueType.name) ..."

                boolean  processIssue = true  // place for optional additional filter

                if (processIssue) {

                    if (!previewMode) {

                        // Update custom fields:

                        //CustomFieldUpdater.forIssue(issue)
                        //        .withAuthor(ACTOR)
                        //        .addItem(targetField, valNew)
                        //        .updateWithHistory()
                        //
                        // reindexIssue(issue)

                        // Update system fields:

                        MutableIssue mIssue = ComponentAccessor.issueManager.getIssueObject(issue.id)
                        mIssue.setSummary("PREFIX: " + issue.summary)
                        ComponentAccessor.issueManager.updateIssue(ACTOR, mIssue, EventDispatchOption.DO_NOT_DISPATCH, /*sendMail*/false)

                        write "  [OK] (issue updated)"
                    } else {
                        write "  [OK] (issue would be updated)"
                    }

                    updatedIssues++

                } else {
                    write "  [OK] (nothing to add)"
                }

                write "\n"
            }
        }
    }


    @Override
    void postCall() {
        writeln ""
        writeln "Issues updated: " + updatedIssues
    }
    
}

//===========================================================================================================//

MigrationScript script = new MigrationScript()


//--[EDIT THIS]--------------------------------------------------
script.ISSUE_BATCH_SIZE = 500
script.coloerdOutput = false
script.runInBackground = false   // pokud je true spusti se ve vlaknu na pozadi (vhodne pro velke >1000 pocty issues)

script.JQL_QUERY = """
 issuetype in ("Epic", "Objective Increment") and labels is not EMPTY ORDER BY id
"""
// Poznamka: Pokud query vraci vic issues nez je BATCH_SIZE, vysledky query (vcetne poradi)
// musi byt invariantni vzhledem k issue updatum provedenym ve skriptu. 

// Uzivatel pod kterym bude videt zmenu v historii
script.ACTOR = ComponentAccessor.userManager.getUserByName('apeman')
// script.ACTOR = null // -> Anonymous

script.previewMode = true        // pokud je true zadne zmeny nebudou zapsany
//---------------------------------------------------------------

result = script.result()






