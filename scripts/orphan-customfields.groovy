import com.atlassian.jira.component.ComponentAccessor
import org.ofbiz.core.entity.GenericValue

/**
 * orphan-customfields.groovy
 *
 *  v 1.1
 *
 * List (and optionally remove) all custom field entries that exist in the database but are not instantiated in Jira
 * (because required plugin that defines the is not installed/enabled)
 *
 * tested on JIRA 6.0.8
 */

//=================================================================//
class g {static String msg = ""}
def print(String s) { log.warn s; g.msg += s }
def println(String s) { print s + "\n" }
//=================================================================//

class Utils {
    static List<GenericValue> getOrphanCustomFields() {
        ComponentAccessor.ofBizDelegator.findAll("CustomField").findAll {
            ComponentAccessor.customFieldManager.getCustomFieldObject(it.get("id")) == null
        }
    }

    static void removeOrphanField(Long id) {
        ComponentAccessor.customFieldManager.removeCustomFieldPossiblyLeavingOrphanedData(id)
    }
}


boolean REMOVE_FIELDS = false

Utils.getOrphanCustomFields().each {
    println "[${it.get("id")}] ${it.get("name")} (${it.get("customfieldtypekey")})"
    if (REMOVE_FIELDS) {
        Utils.removeOrphanField(it.get("id"))
        println "REMOVED"
    }
}

//=================================================================//
return g.msg.replaceAll("\n", "<br>")

