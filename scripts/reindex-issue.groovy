import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.issue.index.IssueIndexingParams

def ISSUE_KEY = "ID-4274" // <-- EDIT THIS!

try {
    ComponentAccessor.getComponent(IssueIndexManager.class).reIndex(ComponentAccessor.issueManager.getIssueObject(ISSUE_KEY), IssueIndexingParams.INDEX_ISSUE_ONLY)
    return "[OK] Reindexed $ISSUE_KEY"
} catch (Exception e) {
    return "[ERROR] Failed to reindex ${ISSUE_KEY}: " + e.message
}
