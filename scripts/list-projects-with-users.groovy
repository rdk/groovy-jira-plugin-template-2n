import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.Project
import com.atlassian.jira.security.roles.ProjectRole
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.user.ApplicationUser

//=================================================================//
class g {static String msg = ""}
def print(String s) { log.warn s; g.msg += s }
def println(String s) { print s + "\n" }
//=================================================================//

boolean isUserInRole(ApplicationUser user, String roleName, Project proj) {
    ProjectRoleManager prm = ComponentAccessor.getComponent(ProjectRoleManager.class);
    ProjectRole role = prm.getProjectRole(roleName);

    if (proj==null || role==null || user==null) return false;

    return prm.isUserInProjectRole(user, role, proj);
}

ApplicationUser user = ComponentAccessor.userManager.getUserByName("username")

List<Project> projects = ComponentAccessor.projectManager.getProjects().findAll { Project proj ->
    !isUserInRole(user, "RoleName", proj)
}


println "DONE"

//=================================================================//
return g.msg.replaceAll("\n", "<br>")
