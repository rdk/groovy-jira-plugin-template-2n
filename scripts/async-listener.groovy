import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.thread.JiraThreadLocalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class AsyncUtils {

    private static final Logger log = LoggerFactory.getLogger(AsyncUtils.class);
    private static final org.apache.log4j.Logger log4j = org.apache.log4j.Logger.getLogger(AsyncUtils.class);


    public static void runAsync(final Runnable action) {
        new AsyncThread() {
            @Override
            void exec() {
                action.run();
            }
        }.start();
    }


    private static abstract class AsyncThread extends Thread {
        private JiraThreadLocalUtil threadUtil = ComponentAccessor.getComponent(JiraThreadLocalUtil.class);

        public AsyncThread() {
            setName(getName() + "-async");
        }

        abstract void exec();

        @Override
        public void run() {
            long start = System.currentTimeMillis();
            log.debug("started async thread: {}", getName());

            threadUtil.preCall(); // establishes context for request caches
            try {
                exec();
            } finally {
                threadUtil.postCall(log4j);
            }

            long time = System.currentTimeMillis() - start;
            log.debug("async thread finished in {} ms", time);
        }
    }
}



AsyncUtils.runAsync({
    // vlastni kod listeneru, treba
    // reindexace issues
})


